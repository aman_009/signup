from flask_wtf import FlaskForm
from wtforms import StringField,PasswordField,SubmitField   
from wtforms.fields.html5 import EmailField
from wtforms.validators import InputRequired,Length,EqualTo,ValidationError,Length
from models import User
from passlib.hash import pbkdf2_sha512


class RegistrationForm(FlaskForm):
    username=StringField('username_label',validators=[InputRequired(message="Username required"),Length(min=4,max=25,message="Username should be 4 to 25")])
    password=PasswordField('password_label',validators=[InputRequired(message="Password required"),Length(min=4,max=25,message="Password should be 4 to 25")])
    confirm_psswd=PasswordField('confirm_pswd',validators=[InputRequired(message="Name required"),EqualTo('password',message="Password must match")])
    submit_button=SubmitField('Create')

    def validate_username(self,username):
        user_object=User.query.filter_by(username=username.data).first()

        if user_object:
            raise ValidationError("Username already exist")

def invalid_credentials(form ,field):
    username_entered=form.username.data
    password_entered=field.data
    user_object=User.query.filter_by(username=username_entered).first()

    if user_object is None:
        raise ValidationError("Username  or password is  wrong")
    elif not pbkdf2_sha512.verify(password_entered,user_object.password):
        raise ValidationError("Username or password is wrong")

class LoginForm(FlaskForm):
    username=StringField('username_label',validators=[InputRequired(message="Username required")])
    password=PasswordField('password_label',validators=[InputRequired(message="Password Required"),invalid_credentials])
    submit_button=SubmitField('Login')